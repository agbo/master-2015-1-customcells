//
//  AGTBookCell.h
//  CustomCell
//
//  Created by Fernando Rodríguez Romero on 30/03/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AGTBookCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *bookIcon;
@property (weak, nonatomic) IBOutlet UIImageView *donwloadIcon;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *authors;


+(NSString*) cellId;

@end
