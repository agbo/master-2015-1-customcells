//
//  AGTBookCell.m
//  CustomCell
//
//  Created by Fernando Rodríguez Romero on 30/03/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTBookCell.h"

@implementation AGTBookCell

+(NSString *) cellId{
    return NSStringFromClass([self class]);
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
