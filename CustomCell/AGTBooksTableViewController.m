//
//  AGTBooksTableViewController.m
//  CustomCell
//
//  Created by Fernando Rodríguez Romero on 30/03/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTBooksTableViewController.h"
#import "AGTBookCell.h"

@interface AGTBooksTableViewController ()

@end

@implementation AGTBooksTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Books for Hackers";
    
    
    // registramos el xib
    UINib *nib = [UINib nibWithNibName:@"AGTBookCell"
                                bundle:[NSBundle mainBundle]];
    
    [self.tableView registerNib:nib
         forCellReuseIdentifier:[AGTBookCell cellId]];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.

    return 30;
}

-(CGFloat) tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 90;
}

-(UITableViewCell*) tableView:(UITableView *)tableView
        cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    

    AGTBookCell *cell = [tableView dequeueReusableCellWithIdentifier:[AGTBookCell cellId]
                                                            forIndexPath:indexPath];
    
    cell.bookIcon.image = [UIImage imageNamed:@"book.png"];
    cell.donwloadIcon.image = [UIImage imageNamed:@"download.png"];
    cell.title.text = @"Structure and Interpretation of Computer Programs";
    cell.authors.text = @"Gerald Jay Sussman and Hal Abelson";
    
    return cell;
}

@end
